-- packer.nvim automatic install
local packerPath = vim.fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"
if vim.fn.empty(vim.fn.glob(packerPath)) > 0 then
    vim.cmd("!git clone https://github.com/wbthomason/packer.nvim " .. packerPath)
    vim.cmd("packadd packer.nvim")
end

vim.cmd("packadd packer.nvim")

require("packer").startup(function(use)
    -- Packer
    use { "wbthomason/packer.nvim", opt = true }

    -- Vim SourceCFG
    use { "https://gitlab.com/ArchB1W/vim-sourcecfg" }

    -- Treesitter
    use {
        "nvim-treesitter/nvim-treesitter",
        requires = {
            "p00f/nvim-ts-rainbow",
            "nvim-treesitter/nvim-treesitter-textobjects",
        },
        run = ":TSUpdate",
    }

    -- Colorscheme
    use("folke/tokyonight.nvim")

    -- Statusline
    use { "nvim-lualine/lualine.nvim", requires = "nvim-tree/nvim-web-devicons" }

    -- Completion
    use {
        "hrsh7th/nvim-cmp",
        requires = {
            -- Snippet Engine
            "hrsh7th/cmp-vsnip",
            "hrsh7th/vim-vsnip",
            -- Buffer
            "hrsh7th/cmp-buffer",
            -- Path
            "hrsh7th/cmp-path",
            -- Command Line
            "hrsh7th/cmp-cmdline",
            -- LSP
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-nvim-lsp-signature-help",
            -- Icons
            "onsails/lspkind.nvim",
            -- Neovim Lua API
            { "hrsh7th/cmp-nvim-lua", ft = "lua" },
        },
    }

    -- LSP
    use {
        "neovim/nvim-lspconfig",
        requires = {
            "williamboman/mason.nvim",
            "WhoIsSethDaniel/mason-tool-installer.nvim",
            "kosayoda/nvim-lightbulb",
            "mfussenegger/nvim-jdtls",
        },
    }

    -- Guess Indent
    use("NMAC427/guess-indent.nvim")

    -- Telescope
    use { "nvim-telescope/telescope.nvim", requires = "nvim-lua/plenary.nvim" }

    -- Formatting
    use("mhartington/formatter.nvim")

    -- Linting
    use("mfussenegger/nvim-lint")

    -- Commenting
    use("numToStr/Comment.nvim")

    -- Indent Guides
    use("lukas-reineke/indent-blankline.nvim")

    -- Harpoon
    use { "ThePrimeagen/harpoon", requires = "nvim-lua/plenary.nvim" }

    -- UndoTree
    use("mbbill/undotree")

    -- Live Command Preview
    use("smjonas/live-command.nvim")
end)
