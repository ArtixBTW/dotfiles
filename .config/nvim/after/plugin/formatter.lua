require("formatter").setup {
    logging = true,
    log_level = vim.log.levels.WARN,
    -- All formatter configurations are opt-in
    filetype = {
        lua = {
            -- "formatter.filetypes.lua" defines defaults for the "lua" filetype
            require("formatter.filetypes.lua").stylua,
        },

        python = {
            -- "formatter.filetypes.python" defines defaults for the "python" filetype
            require("formatter.filetypes.python").autopep8,
        },

        -- Use the "*" filetype for defining formatter configurations on any filetype
        ["*"] = { require("formatter.filetypes.any").remove_trailing_whitespace },
    },
}
