local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

-- Highlight on yank
autocmd("TextYankPost", {
    group = augroup("HighlightYank", {}),
    pattern = "*",
    callback = function()
        vim.highlight.on_yank {
            higroup = "IncSearch",
            timeout = 40,
        }
    end,
})

-- Please fix your plugin to not require restarting it all the time
autocmd("BufWrite", {
    pattern = "*",
    callback = function()
        vim.cmd("TSDisable rainbow | TSEnable rainbow")
    end,
})
