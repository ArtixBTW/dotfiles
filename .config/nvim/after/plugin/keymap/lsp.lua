local map = vim.keymap

-- Mostly stolen from the ThePrimeagen
map.set("n", "[d", vim.diagnostic.goto_prev) -- Next Diagnostics
map.set("n", "]d", vim.diagnostic.goto_next) -- Previous Diagnostics
map.set("n", "gd", vim.lsp.buf.definition) -- Go to Definition
map.set("n", "gca", vim.lsp.buf.code_action) -- Code Actions
map.set("n", "<leader>gws", vim.lsp.buf.workspace_symbol) -- Workspace Symbols
map.set("n", "<leader>gd", vim.diagnostic.open_float) -- Line Diagnostics
map.set("n", "<leader>grn", vim.lsp.buf.rename) -- Rename
map.set("n", "<leader>grr", vim.lsp.buf.references) -- References
map.set("n", "K", vim.lsp.buf.hover) -- Hover
