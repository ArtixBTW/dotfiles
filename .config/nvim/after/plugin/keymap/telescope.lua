local map = vim.keymap

-- Telescope
local telescope_builtin = require("telescope.builtin")
map.set("n", "<C-P>", telescope_builtin.find_files)
map.set("n", "<leader>ff", telescope_builtin.find_files)
map.set("n", "<leader>fg", telescope_builtin.live_grep)
map.set("n", "<leader>fws", telescope_builtin.lsp_workspace_symbols)
map.set("n", "<leader>fds", telescope_builtin.lsp_document_symbols)
map.set("n", "<leader>q", telescope_builtin.diagnostics)
