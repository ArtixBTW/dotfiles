local map = vim.keymap

-- These are for copying to the system clipboard
map.set({ "n", "v" }, "<leader>y", '"+y')
map.set({ "n", "v" }, "<leader>d", '"+d')

-- These are for pasting from the system clipboard
map.set({ "n", "v" }, "<leader>p", '"+p')
map.set({ "n", "v" }, "<leader>P", '"+P')

-- Paste from register with Alt-V
map.set({ "i", "c" }, "<M-v>", '<C-R>"')

-- Move selected line / block of text in normal / visual mode (With autoindent)
map.set("n", "<A-j>", ":m .+1<CR>==")
map.set("n", "<A-k>", ":m .-2<CR>==")
map.set("i", "<A-j>", "<Esc>:m .+1<CR>==gi")
map.set("i", "<A-k>", "<Esc>:m .-2<CR>==gi")
map.set("v", "<A-j>", ":m '>+1<CR>gv=gv")
map.set("v", "<A-k>", ":m '<-2<CR>gv=gv")

-- Escape Terminal
map.set("t", "<ESC>\\", "<C-\\><C-n>")

-- Toggle spellchecking (vim.o makes it local to the buffer/window)
-- stylua: ignore
map.set("n", "<leader><S-S>", function() vim.o.spell = not vim.o.spell end)

-- netrw
map.set("n", "<leader>e", "<CMD>Explore<CR>")

-- Format
map.set("n", "<leader>lf", "<CMD>Format<CR>")
