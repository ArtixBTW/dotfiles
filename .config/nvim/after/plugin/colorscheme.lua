require("tokyonight").setup {
    style = "storm",
    transparent = true,
    sidebars = { "packer" },
}
vim.cmd("colorscheme tokyonight")
