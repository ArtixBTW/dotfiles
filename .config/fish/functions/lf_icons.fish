function lf_icons
    set -gx LF_ICONS $(sed "$XDG_CONFIG_HOME"/diricons \
                -e '/^[ \t]*#/d'       \
                -e '/^[ \t]*$/d'       \
                -e 's/[ \t]\+/=/g'     \
                -e 's/$/ /' \
                | tr '\n' ':' | tr -d ' '
            )
end
