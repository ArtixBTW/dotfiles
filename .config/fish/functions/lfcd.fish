function lfcd
    set -l old_tty (stty -g)
    stty sane

    set -l tmp (mktemp)
    lf -last-dir-path=$tmp $argv
    if test -f "$tmp"
        set -l dir (cat $tmp)
        rm -f $tmp
        if test -d "$dir"
            if test "$dir" != (pwd)
                cd $dir
            end
        end
    end

    stty $old_tty
    commandline -f repaint
end
