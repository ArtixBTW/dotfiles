local util = {}

local hb = hilbish

function util.reversedipairs(t)
    local function reversedipairsiter(t, i)
        i = i - 1
        if i ~= 0 then
            return i, t[i]
        end
    end
    return reversedipairsiter, t, #t + 1
end

function util.hasbin(cmdStr)
    return hb.which(cmdStr) ~= nil
end

-- (trim12)[http://lua-users.org/wiki/StringTrim]
function util.trim(s)
    local from = s:match("^%s*()")
    return from > #s and "" or s:match(".*%S", from)
end

return util
