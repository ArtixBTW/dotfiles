local aliaser = require("lib.aliaser")

local aliasTable = {
    ["7zr"] = {
        { "7zdir", '7zr a "$(basename $PWD)".7z ./' },
    },
    bat = {
        { "cat", "bat -pp" },
    },
    curl = {
        { "wdl", "curl -LO --progress-bar" },
    },
    exa = {
        { "ls", "exa --git --icons --color=auto --group-directories-first" },
        { "ll", "ls -al" },
        { "la", "ls -a | wc -l" },
    },
    dotbare = {
        { "db", "dotbare" },
    },
    git = {
        { "g", "git" },
        { "gb", "g branch" },
        { "gst", "g status" },
        { "gcl", "g clone --recurse-submodules" },
        { "gc", "g commit -v" },
        { "gcm", "gc -m" },
        { "gf", "g fetch" },
        { "gpu", "g pull" },
        { "gd", "g diff" },
        { "ga", "g add" },
        { "grm", "g rm" },
        { "gre", "g restore" },
        { "gres", "gre --staged" },
        { "gp", "g push" },
    },
    less = {
        { "less", "less -R" },
    },
    mpv = {
        { "mpv", "mpv --volume=30" },
    },
    pacman = {
        { "pac", "sudo pacman" },
        { "pacman-unused", "sudo pacman -Rns $(pacman -Qtdq)" },
    },
    pikaur = {
        { "pik", "pikaur" },
    },
    pgrep = {
        { "prsg", "pgrep -afi" },
    },
    reflector = {
        {
            "fetch-mirrors",
            "sudo reflector -c 'United States' -f5 --save /etc/pacman.d/mirrorlist",
        },
    },
    rsync = {
        { "rsyncp", "rsync -ah --progress" },
    },
    ["youtube-dl"] = {
        { "ydl", "youtube-dl --embed-thumbnail --add-metadata -i" },
        { "ydla", "ydl -x -f bestaudio/best" },
    },
    zip = {
        { "zipdir", 'zip -r "$(basename $PWD)".zip ./' },
    },
}

local noneTable = {
    none = {
        { "v", "$EDITOR" },
        { "diff", "diff --color -u" },
        { "gmime", "file --mime-type -bL" },
        { "ls", "ls --color" },
        { "ll", "ls -Al" },
        { "la", "ls -A | wc -l" },
        { "rm", "rm -i" },
        { "mv", "mv -i" },
        { "cp", "cp -i" },
    },
}

aliaser.loadAliases(noneTable)
aliaser.loadAliases(aliasTable)
