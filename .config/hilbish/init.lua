-- Requires hilbish master
local lunacolors = require("lunacolors")
local bait = require("bait")
local ansikit = require("ansikit")

---@diagnostic disable-next-line: lowercase-global
inspect = require("lib.inspect")

local function doPrompt(fail)
    local sshPrompt = ""
    ---@diagnostic disable-next-line: undefined-global
    if (SSH_CLIENT or SSH_TTY) ~= nil then
        sshPrompt = "{blue}%u{white}@{yellow}%h "
    end
    hilbish.prompt(
        lunacolors.format(
            sshPrompt .. "{cyan}%d " .. (fail and "{red}" or "{green}") .. "∆ "
        )
    )
end
doPrompt()

-- This is run whenver a command is exited
bait.catch("command.exit", function(code)
    doPrompt(code ~= 0)
    ansikit.cursorStyle(ansikit.lineCursor)
end)

-- Use pkgfile to try to find the command if it isn't found
bait.catch("command.not-found", function(cmdStr)
    local exit, packages = hilbish.run(
        "pkgfile --binaries --verbose -- " .. cmdStr .. " 2>/dev/null", false
    )
    if exit == 0 then
        print(cmdStr .. " may be found in the following packages:")
        print("  " .. packages)
    end
end)

-- Vim Mode and cursorStyle
hilbish.inputMode("vim")
ansikit.cursorStyle(ansikit.lineCursor)
-- Set cursorStyles for vim mode
bait.catch("hilbish.vimMode", function(mode)
    if mode ~= "insert" then
        ansikit.cursorStyle(ansikit.blockCursor)
    else
        ansikit.cursorStyle(ansikit.lineCursor)
    end
end)

-- TODO Set keybind for hint completion
-- Maybe commander.register
-- editor.insert (setVimRegister)
-- NOTE This is buggy and is incorrect when the completion menu is open
-- function hilbish.hinter(line, pos)
--     for _, v in pairs(hilbish.history.all()) do
--         if string.sub(v, 0, pos) == line then
--             return string.sub(v, pos + 1)
--         end
--     end
-- end

-- TODO Configure Highlighting (line)

-- Disable motd and greeting
hilbish.opts.motd = false
hilbish.opts.greeting = false

-- Set aliases
require("conf/aliases")

-- Run justfetch on startup
hilbish.run("justfetch")
