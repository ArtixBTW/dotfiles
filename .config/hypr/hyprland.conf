# vim:filetype=toml

# TODO Rewrite from updated example config
# Monitors
monitor=DP-3,1920x1080@185.001007,0x0,1
monitor=HDMI-A-1,1920x1080@74.973,1920x0,1
monitor=,preferred,auto,1
# Bind Workspaces
wsbind=1,DP-3
wsbind=2,DP-3
wsbind=3,DP-3
wsbind=4,HDMI-A-1
wsbind=5,HDMI-A-1
wsbind=6,HDMI-A-1
wsbind=7,DP-3
wsbind=8,DP-3
wsbind=9,DP-3
wsbind=10,DP-3

input {
    kb_options=caps:escape

    repeat_rate=50
    repeat_delay=300
    force_no_accel=true

    follow_mouse=1

    sensitivity=0 # -1.0 - 1.0, 0 means no modification.
}

general {
    gaps_in=5
    gaps_out=20
    border_size=2
    col.active_border=rgb(7aa2f7)
    col.inactive_border=rgb(414868)

    cursor_inactive_timeout=5

    apply_sens_to_raw=0 # Whether to apply the sensitivity to raw input (e.g. used by games where you aim using your mouse)
}

misc {
    disable_autoreload=1
    disable_hyprland_logo=1
    disable_splash_rendering=1
}

decoration {
    rounding=10
    blur=1
    blur_size=3 # Minimum 1
    blur_passes=1 # Minimum 1
    blur_new_optimizations=1
}

animations {
    enabled=1
    animation=windows,1,7,default
    animation=border,1,10,default
    animation=fade,1,10,default
    animation=workspaces,1,6,default
}

dwindle {
    pseudotile=0 # Enable pseudotiling on dwindle
}

## Window Rules
windowrulev2=float,class:file_progress
windowrulev2=float,class:confirm
windowrulev2=float,class:dialog
windowrulev2=float,class:download
windowrulev2=float,class:error
windowrulev2=float,class:notification
windowrulev2=float,class:splash
windowrulev2=float,class:toolbar
windowrulev2=float,class:yad
# Applications
windowrulev2=float,class:Steam,title:Friends List
windowrulev2=float,class:Steam,title:Steam - News
windowrulev2=float,class:xdg-desktop-portal-gtk,title:Open Files
windowrulev2=float,class:love

### Binds
## WM
bind=SUPER_SHIFT,c,killactive
bind=SUPER_ALT_SHIFT,q,exit
bind=SUPER_ALT_SHIFT,r,exec,hyprctl reload
bind=SUPER_SHIFT,g,exec,hyprpicker | tr -d "\n" | wl-copy
bind=SUPER_SHIFT,s,exec,justscreen
bind=SUPER_SHIFT,f,bringactivetotop
bind=SUPER,f,fullscreen
bind=SUPER_SHIFT,space,togglefloating,active
# Workspace Keys
bind=SUPER,1,workspace,1
bind=SUPER,2,workspace,2
bind=SUPER,3,workspace,3
bind=SUPER,4,workspace,4
bind=SUPER,5,workspace,5
bind=SUPER,6,workspace,6
bind=SUPER,7,workspace,7
bind=SUPER,8,workspace,8
bind=SUPER,9,workspace,9
bind=SUPER,0,workspace,10

bind=SUPER_SHIFT,1,movetoworkspace,1
bind=SUPER_SHIFT,2,movetoworkspace,2
bind=SUPER_SHIFT,3,movetoworkspace,3
bind=SUPER_SHIFT,4,movetoworkspace,4
bind=SUPER_SHIFT,5,movetoworkspace,5
bind=SUPER_SHIFT,6,movetoworkspace,6
bind=SUPER_SHIFT,7,movetoworkspace,7
bind=SUPER_SHIFT,8,movetoworkspace,8
bind=SUPER_SHIFT,9,movetoworkspace,9
bind=SUPER_SHIFT,0,movetoworkspace,10

bind=SUPER_CTRL,1,movetoworkspacesilent,1
bind=SUPER_CTRL,2,movetoworkspacesilent,2
bind=SUPER_CTRL,3,movetoworkspacesilent,3
bind=SUPER_CTRL,4,movetoworkspacesilent,4
bind=SUPER_CTRL,5,movetoworkspacesilent,5
bind=SUPER_CTRL,6,movetoworkspacesilent,6
bind=SUPER_CTRL,7,movetoworkspacesilent,7
bind=SUPER_CTRL,8,movetoworkspacesilent,8
bind=SUPER_CTRL,9,movetoworkspacesilent,9
bind=SUPER_CTRL,0,movetoworkspacesilent,10
# Alternate Workspace Keys
bind=SUPER,q,workspace,1
bind=SUPER,w,workspace,2
bind=SUPER,e,workspace,3
bind=SUPER,r,workspace,4
bind=SUPER,backslash,workspace,5
bind=SUPER,slash,workspace,6
bind=SUPER,semicolon,workspace,7
bind=SUPER,apostrophe,workspace,8
bind=SUPER,bracketleft,workspace,9
bind=SUPER,bracketright,workspace,10

bind=SUPER_SHIFT,q,movetoworkspace,1
bind=SUPER_SHIFT,w,movetoworkspace,2
bind=SUPER_SHIFT,e,movetoworkspace,3
bind=SUPER_SHIFT,r,movetoworkspace,4
bind=SUPER_SHIFT,backslash,movetoworkspace,5
bind=SUPER_SHIFT,slash,movetoworkspace,6
bind=SUPER_SHIFT,semicolon,movetoworkspace,7
bind=SUPER_SHIFT,apostrophe,movetoworkspace,8
bind=SUPER_SHIFT,bracketleft,movetoworkspace,9
bind=SUPER_SHIFT,bracketright,movetoworkspace,10

bind=SUPER_CTRL,q,movetoworkspacesilent,1
bind=SUPER_CTRL,w,movetoworkspacesilent,2
bind=SUPER_CTRL,e,movetoworkspacesilent,3
bind=SUPER_CTRL,r,movetoworkspacesilent,4
bind=SUPER_CTRL,backslash,movetoworkspacesilent,5
bind=SUPER_CTRL,slash,movetoworkspacesilent,6
bind=SUPER_CTRL,semicolon,movetoworkspacesilent,7
bind=SUPER_CTRL,apostrophe,movetoworkspacesilent,8
bind=SUPER_CTRL,bracketleft,movetoworkspacesilent,9
bind=SUPER_CTRL,bracketright,movetoworkspacesilent,10
# Switch focus between monitors
bind=SUPER,comma,focusmonitor,DP-3
bind=SUPER,period,focusmonitor,HDMI-A-1
# Window Selection
bind=SUPER,h,movefocus,l
bind=SUPER,l,movefocus,r
bind=SUPER,k,movefocus,u
bind=SUPER,j,movefocus,d
bind=SUPER_SHIFT,h,movewindow,l
bind=SUPER_SHIFT,l,movewindow,r
bind=SUPER_SHIFT,k,movewindow,u
bind=SUPER_SHIFT,j,movewindow,d
# Mouse Binds
bindm=SUPER,mouse:272,movewindow
bindm=SUPER,mouse:273,resizewindow
## Everything Else
# Clipboard
bind=SUPER,C,exec,clipman pick -t $GMENU
# Toggle Bar
bind=SUPER_SHIFT,b,exec,killall -SIGUSR1 waybar
# Dismiss Notifications
bind=ALT,space,exec,dunstctl close
# Media Keys
bind=,XF86AudioRaiseVolume,exec,wpctl set-volume @DEFAULT_SINK@ 5%+
bind=,XF86AudioLowerVolume,exec,wpctl set-volume @DEFAULT_SINK@ 5%-
bind=,XF86AudioMute,exec,wpctl set-mute @DEFAULT_SINK@ toggle
bind=,XF86AudioNext,exec,playerctl next
bind=,XF86AudioPlay,exec,playerctl play-pause
bind=,XF86AudioPrev,exec,playerctl previous
# Apps
bind=SUPER,space,exec,j4-dmenu-desktop --dmenu=$GMENU --no-generic
bind=SUPER_ALT,space,exec,gmenu-run-i
bind=SUPER,t,exec,$TERMINAL
bind=SUPER,b,exec,$BROWSER
bind=SUPER_SHIFT,m,exec,pmclaunch
bind=SUPER,p,exec,passmenu-lite

## Startup
# Screensharing Stuff
# Import the WAYLAND_DISPLAY env var from the wm into the systemd user session.
exec-once=dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once=systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
# Polkit
exec-once=/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
# Automatically lock the screen after 3 minutes
exec-once=swayidle -w timeout 180 'swaylock' timeout 181 'playerctl pause'
# Clipboard
exec-once=wl-paste -t text --watch clipman store
# Theming
exec=killall -9 xsettingsd; xsettingsd
exec=import-gsettings
exec-once=hyprctl setcursor Breeze 24
# Background
exec=killall -9 swaybg; swaybg -i ~/.local/share/bg.png
# Waybar
exec=killall -9 waybar; waybar

# CoreCtrl
exec-once=corectrl --minimize-systray
